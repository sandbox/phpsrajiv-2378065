(function($){
$('form.download-report button[type=submit], form.download-report input[type=submit]').each(function(){
  $(this).click(function(){
    var svg = $(this).parents().find('svg')[0];
    var svg_xml = (new XMLSerializer).serializeToString(svg);
    $(this).parents().find('input[name="svg"]').val(svg_xml);  
  });
});
})(jQuery);
