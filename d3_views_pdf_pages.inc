<?php

/**
 * Page callback for downloading report pdf files.
 */
function d3_views_pdf_download_report() {
  $svg_xml = $_POST['svg'];
  $pdf = d3_views_pdf_svg_to_pdf($svg_xml);
  if (!$pdf) {
    return t('An error occured.');
  }
  header('Content-Type: application/pdf');
  header('Content-Length: ' . strlen($pdf));
  header('Content-Disposition: attachment; filename="mydocument.pdf"');
  print $pdf;
  return NULL;
}

/**
 * Generates a pdf file using TCPDF module.
 *
 * @return string Binary string of the generated pdf.
 */
 //@TODO add header and footer text
function d3_views_pdf_svg_to_pdf($svg_xml) {
  $tcpdf = tcpdf_get_instance();
  $tcpdf->DrupalInitialize(array(
    'footer' => array(
      'html' => '',
    ),
    'header' => array(
      'callback' => array(
        'function' => 'd3_views_pdf_default_report_header',
        // You can pass extra data to your callback.
        'context' => array(
          'welcome_message' => '',
        ),
      ),
    ),
  ));

  $tcpdf->ImageSVG('@' . $svg_xml);

  return $tcpdf->Output( 'Report-' . date('F_j_Y-g_i_a') .'.pdf', 'D');
}

/**
 * Callback for generating the header. This function acts like if it overridded
 *   the Header() function of tcpdf class except the tcpdf instance is not $this
 *   but a parameter.
 *
 * @param type $tcpdf TCPDFDrupal instance. It can be used as $this in the
 *   Header() function of a siebling of TCPDF.
 */
function d3_views_pdf_default_report_header(&$tcpdf, $context) {
  // $args contains passed variable...
  $theme_settings = variable_get('theme_' . variable_get('theme_default', '') . '_settings', '');
  if (isset($theme_settings['logo_path']) && (file_exists($theme_settings['logo_path']))) {
    $tcpdf->Image(drupal_realpath($theme_settings['logo_path']), 10, 10, 30, 0, '', variable_get('site_url', ''), '', TRUE, 150, '', FALSE, FALSE, 0, FALSE, FALSE, FALSE);
  }
  $tcpdf->Write(0, $context['welcome_message'], '', 0, 'J', true, 0, false, true, 0);
}
